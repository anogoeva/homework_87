import {Card, CardContent, CardHeader, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import ImageNotSupportedIcon from '@mui/icons-material/ImageNotSupported';
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const PostItem = ({title, author, image, date, id}) => {
    const classes = useStyles();

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>

            <Card className={classes.card}>
                <CardHeader title={title} component={Link} to={'/post/' + id}/>
                <CardHeader date={date}/>
                {image ?
                    <CardMedia
                        image={apiURL + '/' + image}
                        className={classes.media}
                    /> :
                    <ImageNotSupportedIcon sx={{fontSize: 80}}/>
                }
                <CardContent>
                    <Typography><i>{date}</i> by <b>{author}</b></Typography>
                </CardContent>

            </Card>
        </Grid>
    );
};

PostItem.propTypes = {
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    description: PropTypes.string,
    image: PropTypes.string,
    date: PropTypes.string,
    id: PropTypes.string.isRequired,
};

export default PostItem;