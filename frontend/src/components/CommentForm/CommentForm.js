import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {addComment} from "../../store/actions/commentActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const CommentForm = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    const [state, setState] = useState({
        comment: "",
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = {};
        Object.keys(state).forEach(key => {
            formData[key] = state[key];
        });
        formData['post'] = history.location.pathname.split('/')[2];
        dispatch(addComment(formData));
        setState({comment: ""});
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    return (
        <>
            {user ? (
                <Grid
                    container
                    direction="column"
                    spacing={2}
                    component="form"
                    className={classes.root}
                    autoComplete="off"
                    onSubmit={submitFormHandler}
                >
                    <Grid item xs>
                        <TextField
                            fullWidth
                            multiline
                            rows={3}
                            variant="outlined"
                            label="Comment"
                            name="comment"
                            onChange={inputChangeHandler}
                            value={state.comment}
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained">Add comment</Button>
                    </Grid>
                </Grid>
            ) : ""}
        </>
    );
};

export default CommentForm;