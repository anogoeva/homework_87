import React from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createPost} from "../../store/actions/postsActions";
import PostForm from "../../components/PostForm/PostForm";

const NewPost = ({history}) => {
    const dispatch = useDispatch();

    const onSubmit = async postData => {
        await dispatch(createPost(postData));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New post</Typography>
            <PostForm
                onSubmit={onSubmit}
            />
        </>
    );
};

export default NewPost;