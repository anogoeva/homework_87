import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import {
    Box,
    Card,
    CardContent,
    CardMedia,
    CircularProgress,
    Grid,
    makeStyles,
    Paper,
    Typography
} from "@material-ui/core";
import CommentForm from "../../components/CommentForm/CommentForm";
import {apiURL} from "../../config";
import ImageNotSupportedIcon from '@mui/icons-material/ImageNotSupported';
import {fetchComments} from "../../store/actions/commentActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const Post = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const comments = useSelector(state => state.comment.comments);
    const singleLoading = useSelector(state => state.posts.fetchLoading);
    const commentLoading = useSelector(state => state.comment.commentLoading);

    useEffect(() => {
        dispatch(fetchPost(match.params.id));
    }, [dispatch, match.params.id]);

    useEffect(() => {
        dispatch(fetchComments(match.params.id));
    }, [dispatch, match.params.id]);

    return post && (
        <>
            {singleLoading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) :
                <Paper component={Box} p={2}>
                    <Typography variant="subtitle2"><i>Date: {post.date}</i></Typography>
                    <Typography variant="subtitle2">Author: <b>{post.author}</b></Typography>
                    <Typography variant="h6">{post.title}</Typography>

                    {post.image ?
                        <CardMedia
                            style={{height: 0, paddingTop: '56.25%'}}
                            image={apiURL + '/' + post.image}
                            className={classes.media}
                        /> :
                        <ImageNotSupportedIcon sx={{fontSize: 80}}/>
                    }

                    <Typography variant="body1">{post.description}</Typography>
                    <CommentForm/>

                    {commentLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : comments.map(comment => (
                        <div key={comment._id} id={comment._id}>
                            <Card className={classes.card}>
                                <CardContent>
                                    <Typography/>{comment.comment}<Typography/>
                                </CardContent>
                            </Card>
                        </div>
                    ))}
                </Paper>}
        </>
    );
};

export default Post;