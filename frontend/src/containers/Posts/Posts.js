import React, {useEffect} from 'react';
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import PostItem from "../../components/PostItem/PostItem";

const Products = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);
    const fetchLoading = useSelector(state => state.posts.fetchLoading);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Posts</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : posts.map(post => (
                        <PostItem
                            key={post._id}
                            id={post._id}
                            title={post.title}
                            author={post.author}
                            description={post.description}
                            date={post.date}
                            image={post.image}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Products;