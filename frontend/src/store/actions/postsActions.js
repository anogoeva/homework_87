import axios from "axios";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import axiosApi from "../../axiosApi";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, payload: post});
export const fetchPostFailure = () => ({type: FETCH_POST_FAILURE});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = () => ({type: CREATE_POST_FAILURE});

export const fetchPosts = () => {
  return async (dispatch, getState) => {
    try {
      const headers = {
        'Authorization': getState().users.user && getState().users.user.token
      };

      dispatch(fetchPostsRequest());
      const response = await axiosApi.get('/posts', {headers});
      dispatch(fetchPostsSuccess(response.data));
    } catch (error) {
      dispatch(fetchPostsFailure());
      if (error.response.status === 401) {
        toast.warning('You need login!');
      } else {
        toast.error('Could not fetch posts!', {
          theme: 'colored',
          icon: <WarningIcon/>
        });
      }
    }
  };
};

export const fetchPost = id => {
  return async (dispatch, getState) => {
    try {
      const headers = {
        'Authorization': getState().users.user && getState().users.user.token
      };
      dispatch(fetchPostRequest());
      const response = await axiosApi.get('/posts/' + id, {headers});
      dispatch(fetchPostSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostFailure());
    }
  };
};

export const createPost = postData => {
  return async (dispatch, getState) => {
    try {
      const headers = {
        'Authorization': getState().users.user && getState().users.user.token
      };
      dispatch(createPostRequest());
      await axios.post('http://localhost:8000/posts', postData, {headers});
      dispatch(createPostSuccess());
      toast.success('Post created');
    } catch (e) {
      if (e.response.data.error === 'Data not valid') {
        toast.error('Please fill one of the fields: Description or Image');
      }
      dispatch(createPostFailure());
      throw e;
    }
  };
};