import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from "@material-ui/icons/Warning";


export const ADD_COMMENT_REQUEST = 'ADD_COMMENT_REQUEST';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const addCommentRequest = () => ({type: ADD_COMMENT_REQUEST});
export const addCommentSuccess = () => ({type: ADD_COMMENT_SUCCESS});
export const addCommentFailure = () => ({type: ADD_COMMENT_FAILURE});

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = (comments) => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const addComment = comment => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(addCommentRequest());
            const response = await axiosApi.post('/comments', comment, {headers});
            dispatch(addCommentSuccess(response.data));
            toast.success('Comment created');
        } catch (e) {
            dispatch(addCommentFailure());
        }
    };
};

export const fetchComments = (postId) => {
    return async (dispatch) => {
        try {

            dispatch(fetchCommentsRequest());
            const response = await axiosApi.get('/comments?post=' + postId);
            dispatch(fetchCommentsSuccess(response.data));
        } catch (error) {
            dispatch(fetchCommentsFailure());
            toast.error('Could not fetch comments!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    };
};