import {
    ADD_COMMENT_FAILURE,
    ADD_COMMENT_REQUEST,
    ADD_COMMENT_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "../actions/commentActions";

const initialState = {
    commentLoading: false,
    comment: null,
    comments: null
};

const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_COMMENT_REQUEST:
            return {...state, commentLoading: true};
        case ADD_COMMENT_SUCCESS:
            return {
                ...state, commentLoading: false, comment: action.payload
            };
        case ADD_COMMENT_FAILURE:
            return {...state, commentLoading: false};
        case FETCH_COMMENTS_REQUEST:
            return {...state, commentLoading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, commentLoading: false, comments: action.payload};
        case FETCH_COMMENTS_FAILURE:
            return {...state, commentLoading: false};
        default:
            return state;
    }
};

export default commentReducer;