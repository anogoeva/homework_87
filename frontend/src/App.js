import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import NewPost from "./containers/NewPost/NewPost";
import Post from "./containers/Post/Post";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/posts/new" component={NewPost}/>
            <Route path="/post/:id" component={Post}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Switch>
    </Layout>
);

export default App;
