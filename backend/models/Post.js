const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true,
  },
  author: {
    type: String,
    required: true,
  },
  description: {
    type: String,

  },
  image: {
    type: String,

  },
  date: String
});


PostSchema.path('description').required(function () {
  return !this.image
});
PostSchema.path('image').required(function () {
  return !this.description
});

PostSchema.plugin(idvalidator);
const Post = mongoose.model('Post', PostSchema);

module.exports = Post;