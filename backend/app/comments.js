const express = require('express');
const Comment = require('../models/Comment');
const auth = require("../middleware/auth");

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};
    if (req.query.post) {
      query.post = req.query.post;
    }
    const comments = await Comment.find(query).populate('user', 'username').populate('post', 'title');
    res.send(comments);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, async (req, res) => {
  if (!req.body.comment || !req.body.post) {
    return res.status(400).send('Data not valid');
  }

  const commentData = {
    comment: req.body.comment,
    post: req.body.post,
    user: req.user._id
  };

  const comment = new Comment(commentData);

  try {
    await comment.save();
    res.send(comment);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;